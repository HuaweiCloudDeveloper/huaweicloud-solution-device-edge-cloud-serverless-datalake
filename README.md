[TOC]

**解决方案介绍**
===============
该解决方案基于华为云ServerLess技术架构，通过联合伙伴EMQ IoT、华为云函数工作流 FunctionGraph、对象存储服务 OBS、分布式消息服务Kafka版、图引擎服务 GES等服务的组合，实现车联网数据的端侧灵活采集/传输、云端接入/分析/转换，在云端形成Serverless架构大数据湖，并以图谱方式进行车辆故障分析。适用于各种车辆故障排查场景。

解决方案实践详情页面：https://www.huaweicloud.com/solution/implementations/device-edge-cloud-serverless-datalake.html

**架构图**
---------------
![方案架构](./document/device-edge-cloud-serverless-datalake.png)

**架构描述**
---------------

该解决方案将会部署如下资源：

1、创建分布式消息服务 Kafka版专享版实例，并创建 Topic，用于写入车辆数据，为用户提供便捷高效的消息队列。

2、创建一个对象存储服务 OBS桶，用于存储车辆数据。

3、使用函数工作流 FunctionGraph创建3个函数，用于数据转换、导入数据到GES。

4、创建一个API网关 APIG API，将车辆数据生成GES故障图封装为一个API用户调用。

5、创建一个图引擎服务 GES图，用于生成时序图谱，查询关联事件，定位故障点位。

**组织结构**
---------------
``` lua
huaweicloud-solution-device-edge-cloud-serverless-datalake
├── device-edge-cloud-serverless-datalake.tf.json -- 资源编排模板
├── functiongraph
	├── collect_data.py  -- 原始数据转储
	├── parse_data.py  -- 原始数据转csv格式
	├── import_ges
		├── ges_graph.py  --GES数据处理
		├── import_ges.py  --csv数据转换导入GES
```
**开始使用**
---------------
1、查看Kafka。在控制台单击“服务列表”，选择“[分布式消息服务Kafka版](https://console.huaweicloud.com/dms/?engine=kafka&region=cn-north-4&locale=zh-cn#/queue/manager/newKafkaList)”，在“Kafka专享版”页面找到该方案所创建的Kafka实例。单击进入实例管理控制台。

图1 Kafka实例
![Kafka实例](./document/readme-image-001.png)

2、查看Topic。单击Topic管理，向Topic：vehicle_raw_data 中写入消息，请参考[Kafka开发指南-python文档](https://support.huaweicloud.com/devg-kafka/kafka-python.html)，此Topic默认老化时间72小时，到期Topic会自动消失，若逾期请手动创建以vehicle_raw_data命名的Topic。

图2 Topic
![Topic](./document/readme-image-002.png)

图3 查看消息
![查看消息](./document/readme-image-003.png)

3、进入API调试界面。进入华为云[共享版API网关控制台 API管理界面](https://console.huaweicloud.com/apig/?region=cn-north-4&locale=zh-cn#/apig/multiLogical/openapi/list)，查找新建的API依次单击“更多”—>“调试”。

图4 API管理界面
![API管理界面](./document/readme-image-004.png)

4、API调用。方法选用“POST”,在请求体中填入请求体body内容，单击发送请求。

图5 API调用
![API调用](./document/readme-image-005.png)

```
参数说明：
taskID: 为任务ID, 即本次转换的任务ID；
vinList: 为本次任务指定转换的车架号列表，例如值为["DM666666"]，将DM666666车架号的原始数据转换为csv格式上传至GES；
date: 为本次任务指定转换日期的车辆数据，例如值为2023-05-09，把DM666666车架号2023-05-09原始数据转换为csv格式上传至GES；
hours: 为本次任务指定日期下更细精度的小时数，不传则解析全天的原始数据，例如值为["01","02","03","04","05","06"]，把DM666666车架号2023-05-09 01-06小时的原始数据转换为csv格式上传至GES。
```
5、（可选）发布API。进入华为云[共享版API网关控制台 API管理界面](https://console.huaweicloud.com/apig/?region=cn-north-4&locale=zh-cn#/apig/multiLogical/openapi/list)，查找新建的API单击“发布”，并在API发布页面中，单击“发布”发布API。

图6 进入发布API页面
![进入发布API页面](./document/readme-image-006.png)

图7 发布API
![发布API](./document/readme-image-007.png)

6、登录华为云[图引擎服务 GES](https://console.huaweicloud.com/ges/?region=cn-north-4&locale=zh-cn#/GES/instanceMgmt/instanceMain)，单击“图管理”，选择方案本方案创建的图，单击“操作”列的“访问”。

图8 图管理
![图管理](./document/readme-image-008.png)

7、在图引擎编辑器页面，在Gremlin查询区输入查询语句（请参考[Gremlin查询文档](https://support.huaweicloud.com/usermanual-ges/ges_01_0024.html)获取），按“回车”键执行操作。可以通过查询语句获取点、边、属性等信息，并可以对数据进行修改、局部遍历和属性过滤等操作。

图9 查询图数据
![查询图数据](./document/readme-image-009.png)

